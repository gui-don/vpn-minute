## 0.5.1

- fix: fixes installation path in /usr/lib/vpnm instead of /usr/share/vpn-minute

## 0.5.0

- feat: changes `VPNM_APPLICATION_NAME` to `vpnm`
- feat: moves `"/usr/share/$VPNM_APPLICATION_NAME/terraform/"` to `"/usr/share/terraform/$VPNM_APPLICATION_NAME"`
- feat: better requirement checking
- feat: bumps ubuntu default version from 20 to 24; alpine from 3.11 to 3.20
- fix: removes any overriding of $HOME, awful practice

## 0.4.1

- fix: fixes missing terraform destroy upon specific circumstances
- fix: makes connection checking a bit more resilient by checking another host
- fix: autoapprove destroy
- refactor: fixes some linting issues
- chore: pins Terraform provider dependencies

## 0.4.0

- feat: change Terraform default build files to $XDG_RUNTIME_DIR
- feat: pins provider version to 5+, and requires terraform 1.5+
- feat: uses `gp3` as volume type for AWS instances
- fix: resolves ignored "terraform destroy" when the terraform source path is not the default one
- fix: incorrect default region set when performing "terraform destroy" resulting in lingering resources
- fix: makes sure changing env vars around does not prevent vpnm to work
- fix: prevents IPv6 in `allowed ips` when system have ipv6 disabled
- maintenance: updates pre-commit dependencies and lint bash script

## 0.3.4

- fix: fixes wrong pinning in Terraform code

## 0.3.3

- fix: updates terraform lock file to use recent provider that handles more way to authenticate

## 0.3.2

- fix: fixes missing installation lock file

## 0.3.1

- fix: fixes Terraform code for 1.0.0+
- test: fixes tests with PHP 8.0+
- test: fixes archlinux builds

## 0.3.0

- feat: removes support for PHP 7.3
- feat: adds Fetcher infrastructure and first example with InfrastructureProvisioner
- feat: removes HelloWorld example class
- feat: add statefile to be more robust to fail
- fix: wrong VPNM home
- chore: updates dependencies

## 0.2.0

- feat: moves installation script from aur to this project
- feat: adds basic setup for PHP development
- feat: adds dependency scanning capability for CI/CD
- feat: adds dependency injection component for PHP
- chore: updates dependencies
- chore: updates .pre-commit dependencies

## 0.1.0

- feat: first version
